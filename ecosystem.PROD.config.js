//https://pm2.keymetrics.io/docs/usage/expose/

module.exports = {
    name: "PROD - Start2Impact - ReactApp",
    script: "serve",
    whatch:true,
    env: {
        PM2_SERVE_PATH: '.',
        PM2_SERVE_PORT: 5300,
        PM2_SERVE_SPA: 'true',
        PM2_SERVE_HOMEPAGE: '/index.html'
    }
  }